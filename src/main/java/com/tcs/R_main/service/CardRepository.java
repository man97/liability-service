package com.tcs.R_main.service;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tcs.R_main.model.CreditCard;
import com.tcs.R_main.model.Loan;


@Transactional

@Service
public interface CardRepository extends JpaRepository<CreditCard, String>{
	
	public CreditCard save(CreditCard user);
	public List<CreditCard> findAll();
	public List<CreditCard> findCardBycustId (String custId);
	void delete(String custId);
	public CreditCard findOne(String custId);
}

