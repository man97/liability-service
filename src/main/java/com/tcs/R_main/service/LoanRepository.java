package com.tcs.R_main.service;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tcs.R_main.model.CreditCard;
import com.tcs.R_main.model.Loan;

@Transactional

@Service
public interface LoanRepository extends JpaRepository<Loan, String> {
	
	public Loan save(Loan user);
	public List<Loan> findAll();
	public List<Loan> findBycustId (String custId);
	void delete(String custId);
	void deleteAll();
	public Loan findOne(String custId);
	
}
