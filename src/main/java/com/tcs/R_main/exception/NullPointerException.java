package com.tcs.R_main.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


@ResponseStatus(value=HttpStatus.NO_CONTENT, reason="Data value is null") //404
public class NullPointerException extends Exception {

	public NullPointerException(){
		System.out.println("Data Entered is Null");
	}
}
