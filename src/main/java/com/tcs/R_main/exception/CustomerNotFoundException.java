package com.tcs.R_main.exception;

import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.http.HttpStatus;

@ResponseStatus(value=HttpStatus.NOT_FOUND, reason="Customer Not Found") //404
public class CustomerNotFoundException extends Exception {

	public CustomerNotFoundException(String custId){
		System.out.println("Customer Not present!!!");
	}
}
