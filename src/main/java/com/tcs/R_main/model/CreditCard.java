package com.tcs.R_main.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name="creditcard")
public class CreditCard {
	 @Column(name = "billMonth")
     private Date billMonth ;
	 @Column(name = "currentBillCycleAmt") 
	 private double currentBillCycleAmt ;
	 @Column(name = "minBillAmt")
	 private double minBillAmt;
	 @Column(name = "paymentLastDate")
	 private Date paymentLastDate;
	 @Column(name = "totRewardPoints")
	 private double totRewardPoints;
	 @Column(name = "amtAreares")
	 private double amtAreares;
	 @Column(name = "maxCreditLimit")
	 private double maxCreditLimit;
	 @Column(name = "totConsumption")
	 private double totConsumption;
	 @Column(name = "availLimit")
	 private double availLimit;
	 @Column(name = "cashWithdrawalLimit")
	 private double cashWithdrawalLimit;
	 @Column(name = "cashWithdraw")
	 private double cashWithdraw;
	 @Column(name = "availCashWithdrawal")
	 private double availCashWithdrawal;
	 @Column(name = "latestPayDate")
	 private Date latestPayDate;
	 @Column(name = "latestPayAmt")
	 private double latestPayAmt;
	 @Id
	 @Column(name = "custId")
	 private String custId;
	 @Column(name = "payableAmtCurrentCycle")
	 private double payableAmtCurrentCycle;
	 @Column(name = "statementDate")
	 private Date statementDate;
	 @Column(name = "currencyCode")
	 private String currencyCode;
	 public Date getBillMonth() {
		return billMonth;
	 }
	 public void setBillMonth(Date billMonth) {
		this.billMonth = billMonth;
	 }
	 public double getCurrentBillCycleAmt() {
		return currentBillCycleAmt;
	 }
	 public void setCurrentBillCycleAmt(double currentBillCycleAmt) {
		this.currentBillCycleAmt = currentBillCycleAmt;
	 }
	 public double getMinBillAmt() {
		return minBillAmt;
	 }
     public void setMinBillAmt(double minBillAmt) {
		this.minBillAmt = minBillAmt;
	 }
	 public Date getPaymentLastDate() {
		return paymentLastDate;
	 }
	 public void setPaymentLastDate(Date paymentLastDate) {
		this.paymentLastDate = paymentLastDate;
	 }
	 public double getTotRewardPoints() {
		return totRewardPoints;
     }
	 public void setTotRewardPoints(double totRewardPoints) {
		this.totRewardPoints = totRewardPoints;
     }
	public double getAmtAreares() {
		return amtAreares;
	}
	public void setAmtAreares(double amtAreares) {
		this.amtAreares = amtAreares;
	}
	public double getMaxCreditLimit() {
		return maxCreditLimit;
	}
	public void setMaxCreditLimit(double maxCreditLimit) {
		this.maxCreditLimit = maxCreditLimit;
	}
	public double getTotConsumption() {
		return totConsumption;
	}
	public void setTotConsumption(double totConsumption) {
		this.totConsumption = totConsumption;
	}
	public double getAvailLimit() {
		return availLimit;
	}
	public void setAvailLimit(double availLimit) {
		this.availLimit = availLimit;
	}
	public double getCashWithdrawalLimit() {
		return cashWithdrawalLimit;
	}
	public void setCashWithdrawalLimit(double cashWithdrawalLimit) {
		this.cashWithdrawalLimit = cashWithdrawalLimit;
	}
	public double getCashWithdraw() {
		return cashWithdraw;
	}
	public void setCashWithdraw(double cashWithdraw) {
		this.cashWithdraw = cashWithdraw;
	}
	public double getAvailCashWithdrawal() {
		return availCashWithdrawal;
	}
	public void setAvailCashWithdrawal(double availCashWithdrawal) {
		this.availCashWithdrawal = availCashWithdrawal;
	}
	public Date getLatestPayDate() {
		return latestPayDate;
	}
	public void setLatestPayDate(Date latestPayDate) {
		this.latestPayDate = latestPayDate;
	}
	public double getLatestPayAmt() {
		return latestPayAmt;
	}
	public void setLatestPayAmt(double latestPayAmt) {
		this.latestPayAmt = latestPayAmt;
	}
	public String getCustId() {
		return custId;
	}
	public void setCustId(String custId) {
		this.custId = custId;
	}
	public double getPayableAmtCurrentCycle() {
		return payableAmtCurrentCycle;
	}
	public void setPayableAmtCurrentCycle(double payableAmtCurrentCycle) {
		this.payableAmtCurrentCycle = payableAmtCurrentCycle;
	}
	public Date getStatementDate() {
		return statementDate;
	}
	public void setStatementDate(Date statementDate) {
		this.statementDate = statementDate;
	}
	public String getCurrencyCode() {
		return currencyCode;
	}
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	@Override
	public String toString() {
		return "CreditCard [billMonth=" + billMonth + ", currentBillCycleAmt="
				+ currentBillCycleAmt + ", minBillAmt=" + minBillAmt
				+ ", paymentLastDate=" + paymentLastDate + ", totRewardPoints="
				+ totRewardPoints + ", amtAreares=" + amtAreares
				+ ", maxCreditLimit=" + maxCreditLimit + ", totConsumption="
				+ totConsumption + ", availLimit=" + availLimit
				+ ", cashWithdrawalLimit=" + cashWithdrawalLimit
				+ ", cashWithdraw=" + cashWithdraw + ", availCashWithdrawal="
				+ availCashWithdrawal + ", latestPayDate=" + latestPayDate
				+ ", latestPayAmt=" + latestPayAmt + ", custId=" + custId
				+ ", payableAmtCurrentCycle=" + payableAmtCurrentCycle
				+ ", statementDate=" + statementDate + ", currencyCode="
				+ currencyCode + "]";
	} 
	
	
	 

	
	

}
