package com.tcs.R_main.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
	
	@Entity
	@Table(name = "loansummary")
	public class Loan implements Serializable {
		 @Column(name = "loanAccNumber")
		 private  double loanAccNumber;
		 @Id
		 @Column(name = "custId")
		 private String  custId;
		 @Column(name = "facilityNo")
		 private double facilityNo;
		 @Column(name = "facilityAmt")
		 private double facilityAmt;
		 @Column(name = "FXCurrentBal")
		 private double FXCurrentBal;
		 @Column(name = "currencycode")
		 private String currencyCode;
		 @Column(name = "currencyCodeDesc")
		 private String currencyCodeDesc;
		 @Column(name = "productTypeCode")
		 private double productTypeCode;
		 @Column(name = "productSubTypeCode")
		 private double productSubTypeCode;
		 @Column(name = "productCodeDesc")
		 private String productCodeDesc;
		 @Column(name = "currentInterestRate")
		 private float  currentInterestRate;
		 @Column(name = "acctypecode") 
		 private String accTypeCode;
		 @Column(name = "accTypeCodeDesc")
		 private String accTypeCodeDesc;
		 @Column(name = "facilityStartDate")
		 private Date facilityStartDate;
		 @Column(name = "facilityEndDate")
		 private Date facilityEndDate;
		 @Column(name = "nextPaymentDate")
		 private Date nextPaymentDate;
		 @Column(name = "authorDrawingAccNo")
		 private int authorDrawingAccNo;
		 @Column(name = "currentPeriodPrincipalAmt")
		 private double currentPeriodPrincipalAmt;
		 @Column(name = "currentPeriodInterestAmt")
		 private double currentPeriodInterestAmt;
		 @Column(name = "currentPeriodOtherAmt")
		 private double currentPeriodOtherAmt;
		 @Column(name = "currentPeriodRepaymentAmt")
		 private double currentPeriodRepaymentAmt;
		 @Column(name = "branchCode")
		 private int branchCode;
		 @Column(name = "OutstandingLoanAmt")
		 private double OutstandingLoanAmt;
		 @Column(name = "loanType")
		 private double loanType;
		 @Column(name = "loanAmt")
		 private double loanAmt;
		 @Column(name = "loanStartDate")
		 private Date loanStartDate;
		 @Column(name = "loanEndDate")
		 private Date loanEndDate;
		 @Column(name = "accUpdateTimestamp")
		 private String accUpdateTimestamp;
		 @Column(name = "accSyncTimestamp")
		 private String accSyncTimestamp;
		 public Loan(){}
    	 public Loan(double loanAccNumber,String custId,  double facilityNo,
					double facilityAmt, double fXCurrentBal,
					String currencyCode, String currencyCodeDesc,
					double productTypeCode, double productSubTypeCode,
					String productCodeDesc, float currentInterestRate,
					String accTypeCode, String accTypeCodeDesc,
					Date facilityStartDate, Date facilityEndDate,
					Date nextPaymentDate, int authorDrawingAccNo,
					double currentPeriodPrincipalAmt,
					double currentPeriodInterestAmt,
					double currentPeriodOtherAmt,
					double currentPeriodRepaymentAmt, int branchCode,
					double outstandingLoanAmt, double loanType, double loanAmt,
					Date loanStartDate, Date loanEndDate,
					String accUpdateTimestamp, String accSyncTimestamp) {
				super();
				this.custId = custId;
				this.loanAccNumber = loanAccNumber;
				this.facilityNo = facilityNo;
				this.facilityAmt = facilityAmt;
				FXCurrentBal = fXCurrentBal;
				this.currencyCode = currencyCode;
				this.currencyCodeDesc = currencyCodeDesc;
				this.productTypeCode = productTypeCode;
				this.productSubTypeCode = productSubTypeCode;
				this.productCodeDesc = productCodeDesc;
				this.currentInterestRate = currentInterestRate;
				this.accTypeCode = accTypeCode;
				this.accTypeCodeDesc = accTypeCodeDesc;
				this.facilityStartDate = facilityStartDate;
				this.facilityEndDate = facilityEndDate;
				this.nextPaymentDate = nextPaymentDate;
				this.authorDrawingAccNo = authorDrawingAccNo;
				this.currentPeriodPrincipalAmt = currentPeriodPrincipalAmt;
				this.currentPeriodInterestAmt = currentPeriodInterestAmt;
				this.currentPeriodOtherAmt = currentPeriodOtherAmt;
				this.currentPeriodRepaymentAmt = currentPeriodRepaymentAmt;
				this.branchCode = branchCode;
				OutstandingLoanAmt = outstandingLoanAmt;
				this.loanType = loanType;
				this.loanAmt = loanAmt;
				this.loanStartDate = loanStartDate;
				this.loanEndDate = loanEndDate;
				this.accUpdateTimestamp = accUpdateTimestamp;
				this.accSyncTimestamp = accSyncTimestamp;
			}
		public String getCustId() {
			return custId;
		}
		public void setCustId(String custId) {
			this.custId = custId;
		}
		public double getLoanAccNumber() {
			return loanAccNumber;
		}
		public void setLoanAccNumber(double loanAccNumber) {
			this.loanAccNumber = loanAccNumber;
		}
		public double getFacilityNo() {
			return facilityNo;
		}
		public void setFacilityNo(double facilityNo) {
			this.facilityNo = facilityNo;
		}
		public double getFacilityAmt() {
			return facilityAmt;
		}
		public void setFacilityAmt(double facilityAmt) {
			this.facilityAmt = facilityAmt;
		}
		public double getFXCurrentBal() {
			return FXCurrentBal;
		}
		public void setFXCurrentBal(double fXCurrentBal) {
			FXCurrentBal = fXCurrentBal;
		}
		public String getCurrencyCode() {
			return currencyCode;
		}
		public void setCurrencyCode(String currencyCode) {
			this.currencyCode = currencyCode;
		}
		public String getCurrencyCodeDesc() {
			return currencyCodeDesc;
		}
		public void setCurrencyCodeDesc(String currencyCodeDesc) {
			this.currencyCodeDesc = currencyCodeDesc;
		}
		public double getProductTypeCode() {
			return productTypeCode;
		}
		public void setProductTypeCode(double productTypeCode) {
			this.productTypeCode = productTypeCode;
		}
		public double getProductSubTypeCode() {
			return productSubTypeCode;
		}
		public void setProductSubTypeCode(double productSubTypeCode) {
			this.productSubTypeCode = productSubTypeCode;
		}
		public String getProductCodeDesc() {
			return productCodeDesc;
		}
		public void setProductCodeDesc(String productCodeDesc) {
			this.productCodeDesc = productCodeDesc;
		}
		public float getCurrentInterestRate() {
			return currentInterestRate;
		}
		public void setCurrentInterestRate(float currentInterestRate) {
			this.currentInterestRate = currentInterestRate;
		}
		public String getAccTypeCode() {
			return accTypeCode;
		}
		public void setAccTypeCode(String accTypeCode) {
			this.accTypeCode = accTypeCode;
		}
		public String getAccTypeCodeDesc() {
			return accTypeCodeDesc;
		}
		public void setAccTypeCodeDesc(String accTypeCodeDesc) {
			this.accTypeCodeDesc = accTypeCodeDesc;
		}
		public Date getFacilityStartDate() {
			return facilityStartDate;
		}
		public void setFacilityStartDate(Date facilityStartDate) {
			this.facilityStartDate = facilityStartDate;
		}
		public Date getFacilityEndDate() {
			return facilityEndDate;
		}
		public void setFacilityEndDate(Date facilityEndDate) {
			this.facilityEndDate = facilityEndDate;
		}
		public Date getNextPaymentDate() {
			return nextPaymentDate;
		}
		public void setNextPaymentDate(Date nextPaymentDate) {
			this.nextPaymentDate = nextPaymentDate;
		}
		public int getAuthorDrawingAccNo() {
			return authorDrawingAccNo;
		}
		public void setAuthorDrawingAccNo(int authorDrawingAccNo) {
			this.authorDrawingAccNo = authorDrawingAccNo;
		}
		public double getCurrentPeriodPrincipalAmt() {
			return currentPeriodPrincipalAmt;
		}
		public void setCurrentPeriodPrincipalAmt(double currentPeriodPrincipalAmt) {
			this.currentPeriodPrincipalAmt = currentPeriodPrincipalAmt;
		}
		public double getCurrentPeriodInterestAmt() {
			return currentPeriodInterestAmt;
		}
		public void setCurrentPeriodInterestAmt(double currentPeriodInterestAmt) {
			this.currentPeriodInterestAmt = currentPeriodInterestAmt;
		}
		public double getCurrentPeriodOtherAmt() {
			return currentPeriodOtherAmt;
		}
		public void setCurrentPeriodOtherAmt(double currentPeriodOtherAmt) {
			this.currentPeriodOtherAmt = currentPeriodOtherAmt;
		}
		public double getCurrentPeriodRepaymentAmt() {
			return currentPeriodRepaymentAmt;
		}
		public void setCurrentPeriodRepaymentAmt(double currentPeriodRepaymentAmt) {
			this.currentPeriodRepaymentAmt = currentPeriodRepaymentAmt;
		}
		public int getBranchCode() {
			return branchCode;
		}
		public void setBranchCode(int branchCode) {
			this.branchCode = branchCode;
		}
		public double getOutstandingLoanAmt() {
			return OutstandingLoanAmt;
		}
		public void setOutstandingLoanAmt(double outstandingLoanAmt) {
			OutstandingLoanAmt = outstandingLoanAmt;
		}
		public double getLoanType() {
			return loanType;
		}
		public void setLoanType(double loanType) {
			this.loanType = loanType;
		}
		public double getLoanAmt() {
			return loanAmt;
		}
		public void setLoanAmt(double loanAmt) {
			this.loanAmt = loanAmt;
		}
		public Date getLoanStartDate() {
			return loanStartDate;
		}
		public void setLoanStartDate(Date loanStartDate) {
			this.loanStartDate = loanStartDate;
		}
		public Date getLoanEndDate() {
			return loanEndDate;
		}
		public void setLoanEndDate(Date loanEndDate) {
			this.loanEndDate = loanEndDate;
		}
		public String getAccUpdateTimestamp() {
			return accUpdateTimestamp;
		}
		public void setAccUpdateTimestamp(String accUpdateTimestamp) {
			this.accUpdateTimestamp = accUpdateTimestamp;
		}
		public String getAccSyncTimestamp() {
			return accSyncTimestamp;
		}
		public void setAccSyncTimestamp(String accSyncTimestamp) {
			this.accSyncTimestamp = accSyncTimestamp;
		}
		
		@Override
		public String toString() {
			return "Loan [ loanAccNumber="
					+ loanAccNumber + ",custId=" + custId + ", facilityNo=" + facilityNo
					+ ", facilityAmt=" + facilityAmt + ", FXCurrentBal="
					+ FXCurrentBal + ", currencyCode=" + currencyCode
					+ ", currencyCodeDesc=" + currencyCodeDesc
					+ ", productTypeCode=" + productTypeCode
					+ ", productSubTypeCode=" + productSubTypeCode
					+ ", productCodeDesc=" + productCodeDesc
					+ ", currentInterestRate=" + currentInterestRate
					+ ", accTypeCode=" + accTypeCode + ", accTypeCodeDesc="
					+ accTypeCodeDesc + ", facilityStartDate="
					+ facilityStartDate + ", facilityEndDate="
					+ facilityEndDate + ", nextPaymentDate=" + nextPaymentDate
					+ ", authorDrawingAccNo=" + authorDrawingAccNo
					+ ", currentPeriodPrincipalAmt="
					+ currentPeriodPrincipalAmt + ", currentPeriodInterestAmt="
					+ currentPeriodInterestAmt + ", currentPeriodOtherAmt="
					+ currentPeriodOtherAmt + ", currentPeriodRepaymentAmt="
					+ currentPeriodRepaymentAmt + ", branchCode=" + branchCode
					+ ", OutstandingLoanAmt=" + OutstandingLoanAmt
					+ ", loanType=" + loanType + ", loanAmt=" + loanAmt
					+ ", loanStartDate=" + loanStartDate + ", loanEndDate="
					+ loanEndDate + ", accUpdateTimestamp="
					+ accUpdateTimestamp + ", accSyncTimestamp="
					+ accSyncTimestamp + "]";
		}
		 
	
}
