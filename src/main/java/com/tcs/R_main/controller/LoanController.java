package com.tcs.R_main.controller;

import java.awt.PageAttributes.MediaType;
import java.util.List;
import java.util.zip.DataFormatException;

import javax.transaction.Transactional;






import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;










import com.tcs.R_main.exception.CustomerNotFoundException;
import com.tcs.R_main.exception.NullPointerException;
import com.tcs.R_main.model.CreditCard;
import com.tcs.R_main.model.Loan;
import com.tcs.R_main.service.LoanRepository;
import com.tcs.R_main.service.CardRepository;


@RestController
@Transactional
@RequestMapping("/Loan")
public class LoanController {
	
	private static final Logger logger = LoggerFactory.getLogger(LoanController.class);
	
	
@Autowired
LoanRepository loanrepo;
@Autowired
CardRepository cardrepo;

@RequestMapping(value= "/addLoanDetails", method=RequestMethod.POST)
public Loan save(@RequestBody Loan user)
{
	logger.info("In Loan Details Adding");
	System.out.println(user.toString());
	logger.info("Out of Loan Details Adding");
	return loanrepo.save(user);
}



@RequestMapping(value= "/getLoanDetails", method=RequestMethod.GET)
public List<Loan> findAll ()
{
	logger.info("In get Loan details(list) method");
	return loanrepo.findAll();	
	
	
}


@RequestMapping(value="/totalLoan&CreditAmntById/{custId}",method=RequestMethod.GET)
public String getUserAccountDetails(@PathVariable(value="custId") String custId) throws CustomerNotFoundException,NullPointerException
{
	logger.info("In total loan amount");
	
	
	Double totalBal=(double) 0;
	Double loanBal=(double) 0;
	Double cardBal=(double) 0;
	
	
	
	Loan amt1= loanrepo.findOne(custId);
    CreditCard amt2=cardrepo.findOne(custId);
    totalBal=amt1.getLoanAmt()+amt2.getAmtAreares();
	
	System.out.println("Aggregate sum of Loan  & Credit Amount of CustId "+ custId +" is "+ totalBal);
	logger.info("out of total loan amount");
	return  "Total Loan amount is "+ amt1.getLoanAmt()+"\nCard balance is " +amt2.getAmtAreares()+"\n"+"total balance is "+totalBal;
	
}



@RequestMapping(value="/LoanDetailsById/{custId}",method=RequestMethod.GET)
public List<Loan> getLoanAmountDetails(@PathVariable(value="custId") String custId) throws CustomerNotFoundException,NullPointerException
{
	logger.info("In  loan details by ID");
	List<Loan> list=loanrepo.findBycustId(custId);

	if  (list.isEmpty()) {
		throw new CustomerNotFoundException(custId);
	}
	
	if ("custId".isEmpty()){
		throw new NullPointerException();
	}
	System.out.println("Loan Details with CustId "+ custId + "retrived successfully!!!");
	logger.info("Out of  loan details by ID");
	return loanrepo.findBycustId(custId);
	
}



@RequestMapping(value="/updateLoanDetails" ,method=RequestMethod.PUT)
public Loan UpdateUsers(@RequestBody Loan user)
{
	logger.info("In  update loan details ");
	System.out.println(user.toString());
	System.out.println("Loan Details updated successfully!!!");
	logger.info("Out of  update loan details ");
	return loanrepo.save(user);
}


@RequestMapping(value="/deleteLoan/{custId}",method=RequestMethod.DELETE)
public void deleteUser( @PathVariable(value="custId")String custId)
{
	logger.info("In  delete loan details ");
	loanrepo.delete(custId);
	System.out.println("Loan Details with CustId "+ custId + "deleted successfully!!!");
	logger.info("Out of   delete loan details ");
}
@RequestMapping(value="/deleteAll",method=RequestMethod.DELETE)
public void deleteUser()
{
	logger.info("In  delete all loan account ");
	System.out.println("All Loan Details are deleted successfully!!!");
	logger.info("Out of  delete all loan account ");
	 loanrepo.deleteAll();
}


}
