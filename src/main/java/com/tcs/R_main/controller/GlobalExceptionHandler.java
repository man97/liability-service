package com.tcs.R_main.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.thoughtworks.xstream.core.BaseException;

@ControllerAdvice
@RestController
public class GlobalExceptionHandler {
    @ResponseStatus(value=HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value=BaseException.class)
    public String HandleBaseException(BaseException e)
    {
    	System.out.println("Request method is not supported"+e.getMessage());
		return e.getMessage();
    	
    }
    
    @ResponseStatus(value=HttpStatus.CONFLICT)
    @ExceptionHandler(value=HttpMessageNotReadableException.class)
    public String HandleException(HttpMessageNotReadableException e)
    {
    	System.out.println("number format exception#####"+ e.getMessage());
		return e.getMessage();
    	
    }
    
    public String handleException(NullPointerException e) {
    	System.out.println("null pointer exception"+e.getMessage());
		return e.getMessage();
		
	}
    
}
