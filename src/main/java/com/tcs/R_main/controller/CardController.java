package com.tcs.R_main.controller;

import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tcs.R_main.exception.CustomerNotFoundException;
import com.tcs.R_main.model.CreditCard;
import com.tcs.R_main.model.Loan;
import com.tcs.R_main.service.CardRepository;


@RestController
@Transactional
@RequestMapping("/Card")
public class CardController {

	@Autowired
	CardRepository cardrepo;
	
	
	private static final Logger logger = LoggerFactory.getLogger(LoanController.class);
	
	@RequestMapping(value= "/addCardDetails", method=RequestMethod.POST)
	public CreditCard save(@RequestBody CreditCard user)
	{
		logger.info("In  card deails adding ");
		System.out.println(user.toString());
		System.out.println("Card Details are added Successfully!!!");
		logger.info("Out of  card deails adding ");
		return cardrepo.save(user);
		
		
	}

	@RequestMapping(value= "/getCardDetails", method=RequestMethod.GET)
	public List<CreditCard> findAll ()
	{
		logger.info("In  card deails List ");
		return cardrepo.findAll();	
		
	}


	@RequestMapping(value="/getCardDetailsById/{custId}",method=RequestMethod.GET)
	public List<CreditCard> getUserAccountDetails(@PathVariable(value="custId") String custId) throws CustomerNotFoundException
	{
		logger.info("In  card deails by ID ");
		
		List<CreditCard> list=cardrepo.findCardBycustId(custId);

		if (list.isEmpty()) {
			throw new CustomerNotFoundException(custId);
		}
		System.out.println("Card Details with CustId "+ custId + " retrived successfully!!!");
				
		logger.info("Out of  card deails by ID ");
		return cardrepo.findCardBycustId(custId);
		
	}

	@RequestMapping(value="/updateCarDetails" ,method=RequestMethod.PUT)
	public CreditCard UpdateUsers(@RequestBody CreditCard user)
	{
		logger.info("In  update card deails");
		System.out.println(user.toString());
		System.out.println("Card Details updated successfully!!!");
		logger.info("Out of   update card deails");
		return cardrepo.save(user);
	}


	@RequestMapping(value="/deleteCard/{custId}",method=RequestMethod.DELETE)
	public void deleteCard( @PathVariable(value="custId")String custId)
	{
		logger.info("In  delete card deails");
		cardrepo.delete(custId);
		
		System.out.println("Card Details with CustId "+ custId + "deleted successfully!!!");
		logger.info("out of  delete card deails");
	}


	
	/*@RequestMapping(value="/deleteAllCards",method=RequestMethod.DELETE)
	public void deleteCard()
	{
		System.out.println("All Card Details are deleted successfully!!!");
		 cardrepo.deleteAll();
	}*/


	
}
